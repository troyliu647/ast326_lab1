# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 13:30:08 2019

@author: 岩松
"""
import matplotlib.pyplot as plt
import numpy as np

raw = np.loadtxt("step2.txt")

data = []
for l in raw:
    data.extend(l)

def mean(l:list) -> float:
    return np.mean(data)

def sd(l:list) -> float:
    d = 0
    m = mean(l)
    for n in l:
        d += (n - m)**2
    return np.sqrt(d / (len(l) - 1))

print("Mean " + u"\u00B1" + " STDDEV = 13.2 " + u"\u00B1" + " 3.8 (count/second)")

plt.hist(data, 8)
plt.show()

poisson_data = np.random.poisson(12, len(data))
plt.hist(poisson_data, 8)
plt.show()

        
