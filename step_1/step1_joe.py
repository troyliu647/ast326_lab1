# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 23:39:33 2019

@author: zhouk
"""
import matplotlib.pyplot as plt
import numpy as np
from typing import *


def standard_dev(x: List[Union[float, int]]) -> float:
    """Returns the Standard deviation of data array x.
    >>> x = [0,0,0]
    >>> std_p(x)
    0.0
    """
    mean = np.mean(x)
    tot = sum([np.square(val-mean) for val in x])
    return np.sqrt(tot/(len(x) - 1))


def weight_mean_and_error(dataset: List[Union[int, float]], error: List[float]) -> Tuple[float, float]:
    """Return the weighted mean of the data set based on the error array.
    Preconditions:
    => element in the error array are the error of data point at corresponding index.
    => data set and error has the same length
    """
    weighted_sum = 0
    final_error_inv = 0
    for i in range(len(dataset)):
        weight = np.square(1/error[i])
        weighted_sum += weight * dataset[i]
        final_error_inv += weight
    return weighted_sum / final_error_inv, np.sqrt(1 / final_error_inv)


if __name__ == "__main__":
    arr = np.loadtxt('step1.txt', dtype=str, delimiter=")")
    arr = arr.reshape(1, arr.size)
    distance, uncertainty = [], []
    for points_info in arr[0]:
        if points_info:
            points = points_info.split(' (')
            distance.append(float(points[0]))
            uncertainty.append(float(points[1]))

    mean = np.mean(distance)
    umean = standard_dev(distance)
    weighted_mean = weight_mean_and_error(distance,uncertainty)[0]
    weighted_SD = weight_mean_and_error(distance,uncertainty)[1]
    
    
    #print answers#
    
    print("Mean " + u"\u00B1" + " STDDEV = " + str(round(mean, 2)) + " " + u"\u00B1" + " " + str(round(umean, 2))+ " (pc)")
    
    print("(Weighted) Mean " + u"\u00B1" + " STDDEV = " + str(round(weighted_mean, 2)) + " " + u"\u00B1" + " " + str(round(weighted_SD, 2))+ " (pc)")

    """============weight data============"""

    weighted_mean, final_error = weight_mean_and_error(distance, uncertainty)

    xaxis = np.linspace(1, len(distance), len(distance))

    plt.plot(xaxis, distance, 'o')
    plt.xlabel('Measurement')
    plt.ylabel('Distance(PC)')
    plt.show()
    plt.figure()
    plt.hist(distance)
    plt.xlabel('Distance(PC)')
    plt.ylabel('Quantity')
    plt.show()
